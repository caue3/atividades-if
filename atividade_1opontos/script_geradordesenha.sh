#!/bin/bash

# Função para exibir a caixa de diálogo para escolher as opções de senha
show_password_dialog() {
  # Criar um arquivo temporário para armazenar as opções de senha
  options_file=$(mktemp)

  # Exibir a caixa de diálogo usando o Yad
  yad --form \
    --title "Gerador de Senhas" \
    --text "Escolha as opções de senha" \
    --field "Tamanho da senha:NUM" \
    --field "Incluir letras maiúsculas:CHK" \
    --field "Incluir letras minúsculas:CHK" \
    --field "Incluir números:CHK" \
    --field "Incluir caracteres especiais:CHK" \
    --button="Gerar Senhas":0 \
    --button="Cancelar":1 \
    --width=300 \
    --center \
    > "$options_file"

  # Verificar o código de saída do Yad
  exit_code=$?

  # Ler as opções de senha do arquivo temporário
  password_length=$(awk -F'|' '{print $1}' "$options_file")
  include_uppercase=$(awk -F'|' '{print $2}' "$options_file")
  include_lowercase=$(awk -F'|' '{print $3}' "$options_file")
  include_numbers=$(awk -F'|' '{print $4}' "$options_file")
  include_special_chars=$(awk -F'|' '{print $5}' "$options_file")

  # Verificar se o usuário pressionou o botão "Cancelar"
  if [ $exit_code -eq 1 ]; then
    exit
  fi

  # Verificar se o tamanho da senha é válido
  if [ -z "$password_length" ] || [ "$password_length" -lt 1 ]; then
    yad --error --text "Tamanho da senha inválido!"
    exit
  fi

  # Verificar se pelo menos uma opção de caractere foi selecionada
  if [ "$include_uppercase" != "TRUE" ] && [ "$include_lowercase" != "TRUE" ] && [ "$include_numbers" != "TRUE" ] && [ "$include_special_chars" != "TRUE" ]; then
    yad --error --text "Selecione pelo menos uma opção de caractere!"
    exit
  fi

  # Gerar a senha com base nas opções selecionadas
  generate_password
}

# Função para gerar a senha
generate_password() {
  # Criar um arquivo temporário para armazenar os caracteres permitidos
  charset_file=$(mktemp)

  # Definir os caracteres permitidos com base nas opções selecionadas
  charset=""
  if [ "$include_uppercase" = "TRUE" ]; then
    charset="$charsetABCDEFGHIJKLMNOPQRSTUVWXYZ"
  fi
  if [ "$include_lowercase" = "TRUE" ]; then
    charset="$charsetabcdefghijklmnopqrstuvwxyz"
  fi
  if [ "$include_numbers" = "TRUE" ]; then
    charset="$charset0123456789"
  fi
  if [ "$include_special_chars" = "TRUE" ]; then
    charset="$charset!@#$%^&*()_-+=<>?/:;{}[]|"
  fi

  # Salvar os caracteres permitidos no arquivo temporário
  echo "$charset" > "$charset_file"

  # Gerar a senha aleatória usando os caracteres permitidos
  password=$(cat "$charset_file" | grep -o . | shuffle | head -n "$password_length" | tr -d '\n')

  # Exibir a senha gerada
  yad --info --text "Senhas geradas:
$password" --width=300 --center
}

# Exibir a caixa de diálogo para escolher as opções de senha
show_password_dialog
